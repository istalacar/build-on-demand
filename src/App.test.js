import React from "react";
import { mount, shallow } from "enzyme";
import App from "./App";

// Syntactic sugar, see: https://github.com/facebook/jest/issues/2157#issuecomment-279171856
// something like this will maybe added to the Jest API
function flushPromises() {
  return new Promise((resolve) => setImmediate(resolve));
}

it("loads default project form", () => {
  const componentUnderTest = shallow(<App />);
  return flushPromises().then(() => {
    expect(componentUnderTest.state().error).toEqual(null);
  });
});
